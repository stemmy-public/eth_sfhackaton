export interface TournamentData {
  TournamentId: '983629';
  Score: number;
}

const proofRequest: protocol.ZKPRequest = {
  id: 1,
  circuit_id: 'credentialAtomicQuerySig',
  rules: {
    query: {
      allowedIssuers: ['11eJJ28Mic5H5wgdAqfy4BTcku3sYqfnK4TVxaCHy'],
      schema: {
        type: 'TournamentData',
        url: 'https://s3.eu-west-1.amazonaws.com/polygonid-schemas/2e47d4be-e014-449d-9ab9-0c8488d807a5.json-ld',
      },
      req: {
        Score: {
          $gt: 800, // Score must be more than 800 (because 50th player score is 800)
        },
      },
    },
  },
};
