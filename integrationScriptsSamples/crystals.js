window.onload = function () {
  crystalsSpawn();
};

const crystalsSpawn = () => {
  var amountCrystals = 4;
  var maxAmountCrystals = 16;
  var realDocumentWidth = $(document).width();
  var realDocumentHeight = $(document).height();
  var possibleWidth = [
    0.1 * realDocumentWidth,
    realDocumentWidth - 0.1 * realDocumentWidth,
  ];
  var possibleHeight = [
    0.1 * realDocumentHeight,
    realDocumentHeight - 0.1 * realDocumentHeight,
  ];
  window.amountCrystalsChecked = 0;
  var targetPosition = $('#stemmy-123').position();
  var targetWidth = $('#stemmy-123').outerWidth();
  var targetHeight = $('#stemmy-123').outerHeight();
  var elementWidth = 60;
  var elementHeight = 50;

  var amount = $('<div />', {
    style: `
      z-index: 40;
      position: fixed;
      top: 10%;
      right: 3%;
      color: white;
      background: gray;
      padding: 5px;
      padding-left: 10px;
      padding-right: 10px;
      border-radius: 15px;
    `,
    text: 'Amount: 0',
  });
  $('body').append(amount);
  ///////////////////////////////////

  var firstCrystal = $('<img />', {
    style: `transition: all ease-out; width: 60px; z-index: 9999; position: absolute; will-change: transform; cursor: pointer;`,
    src: `/../assets/crystals/${randomInteger(1, 16)}.png`,
  });

  firstCrystal.css({
    top: 0,
    left: 0,
    transform: `translate(${
      targetPosition.left + targetWidth / 2 - elementWidth / 2
    }px, ${targetPosition.top + targetHeight / 2 - elementHeight / 2}px)`,
  });

  $(firstCrystal).on('click', () => {
    window.amountCrystalsChecked += 1;
    $(firstCrystal).css({
      transform: `translate(${$(amount).position().left}px, ${
        $(amount).position().top + $(document).scrollTop()
      }px)`,
      'transition-duration': '0.3s',
    });
    setTimeout(() => {
      $(firstCrystal).css({
        display: 'none',
      });
    }, 300);

    $(amount).text(`Amount: ${window.amountCrystalsChecked}`);
  });

  $('body').append(firstCrystal);

  for (let i = 1; i <= amountCrystals; i++) {
    var element = $('<img />', {
      style: `transition: all ease-out; width: 60px; z-index: 9999; position: absolute; will-change: transform; cursor: pointer;`,
      src: `/../assets/crystals/${
        i > maxAmountCrystals ? randomInteger(1, 16) : i
      }.png`,
    });

    element.css({
      top: 0,
      left: 0,
      transform: `translate(${randomInteger(
        possibleWidth[0],
        possibleWidth[1],
      )}px, ${randomInteger(possibleHeight[0], possibleHeight[1])}px)`,
    });

    element.attr('data-info', i);

    $(element).on('click', () => {
      window.amountCrystalsChecked += 1;
      $(`[data-info="${i}"]`).css({
        transform: `translate(${$(amount).position().left}px, ${
          $(amount).position().top + $(document).scrollTop()
        }px)`,
        'transition-duration': '0.3s',
      });
      setTimeout(() => {
        $(`[data-info="${i}"]`).css({
          display: 'none',
        });
      }, 300);

      $(amount).text(`Amount: ${window.amountCrystalsChecked}`);
    });

    $('body').append(element);
  }
  ///////////////////////////////////
};

// ====================== Utils ======================
function randomInteger(min, max) {
  let rand = min - 0.5 + Math.random() * (max - min + 1);
  return Math.round(rand);
}
