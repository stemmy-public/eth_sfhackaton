window.onload = function () {
  //spawnChest('#stemmy-123');
  // jetpackGuide();
  crystalsSpawn();
};

const spawnChest = (attachTo) => {
  const targetButton = $(attachTo);

  var element = $('<iframe />', {
    id: 'stemmy-chest',
    src: 'http://components.stemmy.io/components/chest',
    style: `width: 30%; height: 30%; z-index: -9999; position: absolute;`,
  });

  var targetPosition = targetButton.position();
  var targetWidth = targetButton.outerWidth();
  var targetHeight = targetButton.outerHeight();

  var elementWidth = $(window).width() * 0.3;
  var elementHeight = $(window).height() * 0.3;

  console.log(elementWidth);

  element.css({
    //top: targetPosition.top + targetHeight / 2 - elementHeight / 2,
    top: targetPosition.top - elementHeight / 2,
    left: targetPosition.left + targetWidth / 2 - elementWidth / 2,
    opacity: 0,
  });

  targetButton.on('click', () => {
    element.css({opacity: 1, zIndex: 40});
  });

  $('body').append(element);
};

// ====================== Utils ======================

const getDistance = (aX, aY, bX, bY) => {
  return Math.sqrt(Math.pow(bX - aX, 2) + Math.pow(bY - aY, 2));
};

const lerp = (a, b, t) => {
  var len = a.length;
  if (b.length != len) return;

  var x = [];
  for (var i = 0; i < len; i++) x.push(a[i] + t * (b[i] - a[i]));
  return x;
};
