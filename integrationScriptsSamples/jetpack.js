var element;
const elementWidth = 200;
const elementHeight = 200;

window.addEventListener('message', (e) => {
  //   console.log(e.data.target);
  //   console.log(e.data.element);
  //   console.log(e.data.event);
  if (e.data.target === 'stemmy-script' && e.data.element === 'jetpack') {
    if (e.data.event.event === 'start') {
      console.log('Starting');
      startFollowMouse();
    }
  }
});

const jetpackGuide = () => {
  element = $('<iframe />', {
    id: 'testid',
    src: 'http://components.stemmy.io/components/jetpack',
    style: `transition: all ease-out; z-index: 40; position: absolute; will-change: transform;`,
    css: {
      width: elementWidth,
      height: elementHeight,
    },
    position: $('#stemmy-123').position(),
  });

  var targetPosition = $('#stemmy-123').offset();
  var targetWidth = $('#stemmy-123').outerWidth();
  var targetHeight = $('#stemmy-123').outerHeight();

  window.pageY = 0;

  element.css({
    top: 0,
    left: 0,
    transform: `translate(${
      targetPosition.left + targetWidth / 2 - elementWidth / 2
    }px, ${targetPosition.top - elementHeight}px)`,
  });

  $('body').append(element);
};

var targetPosition;
var isProhibitFollowMouse = true;

const onMouseMove = (e) => {
  if (isProhibitFollowMouse) return;

  window.pageY = e.clientY;

  var left = 0;
  var top = 0;

  if ($(document).width() <= e.pageX + element.width() + element.width() / 4) {
    left = e.pageX - element.width() / 4 - element.width();
    top = e.pageY - element.height() / 2;
  } else {
    left = e.pageX + element.width() / 4;
    top = e.pageY - element.height() / 2;
  }

  checkCoordinates(e.pageX, e.pageY);

  element.css({
    transform: `translate(${left}px, ${top}px)`,
    'transition-duration': '0.3s',
  });
};

const onScroll = (e) => {
  if (isProhibitFollowMouse) return;

  const guyPos = element.position();
  const scroll = $(document).scrollTop();

  const left = guyPos.left;
  const top = scroll + window.pageY - element.height() / 2;

  checkCoordinates(left, scroll + window.pageY);

  element.css({
    transform: `translate(${left}px, ${top}px)`,
    'transition-duration': '0.3s',
  });
};

const flyToTarget = () => {
  console.log('set fly to target');
  setTimeout(() => {
    var targetWidth = $('#stemmy-123').outerWidth();
    var targetHeight = $('#stemmy-123').outerHeight();

    element.css({
      transform: `translate(${
        targetPosition.left + targetWidth / 2 - elementWidth / 2
      }px, ${targetPosition.top - elementHeight}px)`,
      //   transform: `translate(${targetPosition.left}px, ${targetPosition.top}px)`,
      'transition-duration': '1s',
    });

    setTimeout(() => {
      element[0].contentWindow.postMessage(
        {
          target: 'stemmy-iframe',
          element: 'jetpack',
          event: {
            event: 'end',
          },
        },
        '*',
      );
    }, 800);
  }, 200);
};

const startFollowMouse = () => {
  isProhibitFollowMouse = false;
  targetPosition = $('#stemmy-target').offset();
  $(document).on('mousemove', onMouseMove);
  $(document).on('scroll', onScroll);
};

const endFollowMouse = () => {
  isProhibitFollowMouse = true;
  $(document).remove;
  $(document).off('mousemove', onMouseMove);
  $(document).off('scroll', onScroll);
};

const checkCoordinates = (left, top) => {
  const dist = getDistance(targetPosition.left, targetPosition.top, left, top);

  if (dist < 300) {
    endFollowMouse();
    flyToTarget();
  }
};
