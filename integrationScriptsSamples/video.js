var tag = document.createElement('script');

window.onload = function () {
  tag.src = 'https://www.youtube.com/iframe_api';
  var firstScriptTag = document.getElementsByTagName('script')[0];
  firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
};

var durationTime = 0;
var needDuration = 5;
var isFuncDurationTrigered = false;

const changeDurationTimeFunc = (time) => {
  durationTime += +time.toFixed(1);
  if (durationTime > needDuration && !isFuncDurationTrigered) {
    changeIsFuncDurationTrigered();
    funcAfterDurationTrigger();
  }
};

const funcAfterDurationTrigger = () => {
  console.log('5 sec passed');
};

//
const changeIsFuncDurationTrigered = () => {
  isFuncDurationTrigered = true;
};
///////////////////////////

var player;
function onYouTubeIframeAPIReady() {
  player = new YT.Player('stemmy-youtube', {
    events: {
      onReady: (e) => {
        e.target.playVideo();
      },
      onStateChange: (e) => {
        onPlayerStateChange(e);
      },
    },
  });
}
var timeToTriggerFunc = 3,
  triggerFuncTimer,
  timeListener;
isVideoStarted = false;

function onPlayerStateChange(event) {
  var time, rate, remainingTime;
  clearTimeout(triggerFuncTimer);
  clearInterval(timeListener);
  if (event.data === YT.PlayerState.PLAYING) {
    if (!isVideoStarted) startFunc();
    time = player.getCurrentTime();
    if (time < timeToTriggerFunc) {
      rate = player.getPlaybackRate();
      remainingTime = (timeToTriggerFunc - time) / rate;
      triggerFuncTimer = setTimeout(funcAtCurrentTime, remainingTime * 1000);
    }
    timeListener = setInterval(() => {
      changeDurationTimeFunc(0.1);
    }, 100);
  }
}

function funcAtCurrentTime() {
  console.log(`Ф-ия сработала в ${timeToTriggerFunc} сек`);
}

const startFunc = () => {
  isVideoStarted = true;
  console.log('метод который тригирится при старте видео');
};

// ====================== Utils ======================
function randomInteger(min, max) {
  let rand = min - 0.5 + Math.random() * (max - min + 1);
  return Math.round(rand);
}
